package uz.ort.api.routers;

import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uz.ort.api.controller.DemoController;

public class DemoRouter extends RoutingService {

    private static final Logger LOGGER = LoggerFactory.getLogger("DemoRouter");


    private DemoController demoController;

    public DemoRouter(Vertx vertx) {
        this.router = Router.router(vertx);
        this.demoController = new DemoController(vertx);
        configureUrlMap();
    }

    @Override
    public Router getRouter() {
        return this.router;
    }


    private void configureUrlMap() {
        router.route().handler(BodyHandler.create());
        router.route("/getList").handler(demoController::getList);
    }


}
