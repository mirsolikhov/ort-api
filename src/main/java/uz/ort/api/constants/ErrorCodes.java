package uz.ort.api.constants;

public enum ErrorCodes {
    NO_ACTION_SPECIFIED(1),
    BAD_ACTION(2),
    DB_ERROR(3);

    private int val;

    ErrorCodes(int val) {
        this.val = val;
    }

    public int val() {
        return val;
    }
}
