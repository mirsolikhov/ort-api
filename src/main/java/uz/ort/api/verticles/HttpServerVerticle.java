package uz.ort.api.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uz.ort.api.routers.MainApiRouter;
import uz.ort.api.routers.DemoRouter;

public class HttpServerVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger("HttpServerVerticle");

    @Override
    public void start(Future<Void> future) {
        LOGGER.info("Starting WebServerVerticle ...");

        Router router = new MainApiRouter().createRouting(vertx);
        router.mountSubRouter("/api/demo", new DemoRouter(vertx).getRouter());

        // enable http compression (e.g. gzip js)
        final HttpServerOptions options = new HttpServerOptions()
                .setCompressionSupported(config().getBoolean("enable.http.compression", true));

        // HTTP server settings
        String serverHost = config().getString("server.host", "127.0.0.1");
        Integer serverPort = config().getInteger("server.port", 8100);

        // Start HTTP server
        HttpServer httpServer = vertx.createHttpServer(options);
        httpServer
                .requestHandler(router::accept)
                .listen(serverPort,
                        serverHost,
                        result -> {
                            if (result.succeeded()) {
                                LOGGER.info("WebServerVerticle Started on http://" + serverHost + ":" + httpServer.actualPort());
                                future.complete();
                            } else {
                                LOGGER.error("WebServerVerticle NOT Started " + result.cause().getMessage());
                                future.fail(result.cause());
                            }
                        });
    }

    @Override
    public void stop() {
        LOGGER.info("Shutting down WebServerVerticle...");
    }

}
