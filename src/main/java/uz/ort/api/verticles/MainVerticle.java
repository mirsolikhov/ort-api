package uz.ort.api.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uz.ort.api.utils.ConfigService;

public class MainVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger("MainVerticle");

    @Override
    public void start(Future<Void> startFuture) {
        LOGGER.info("Reading configuration...");
        ConfigService configService = new ConfigService("conf");
        configService
                .readConfig(vertx,
                        res -> {
                            LOGGER.info("Config ready");
                            JsonObject config = res.result();
                            deployUserDatabaseVerticle(config);
                        }
                );
    }

    private void deployUserDatabaseVerticle(JsonObject config) {
        LOGGER.info("Deploying DemoDatabaseVerticle...");
        vertx.deployVerticle(new DemoDatabaseVerticle(),
                new DeploymentOptions().setConfig(config),
                res -> {
                    if (res.succeeded()) {
                        LOGGER.info("DemoDatabaseVerticle deployed SUCCESSFULLY");
                        deployHttpServerVerticle(config);
                    } else {
                        LOGGER.info("DemoDatabaseVerticle NOT deployed !!! " + res.cause());
                    }
                });
    }

    private void deployHttpServerVerticle(JsonObject config) {
        LOGGER.info("Deploying HttpServerVerticle...");
        vertx.deployVerticle(new HttpServerVerticle(),
                new DeploymentOptions().setConfig(config),
                res -> {
                    if (res.succeeded()) {
                        LOGGER.info("HttpServerVerticle deployed SUCCESSFULLY");
                    } else {
                        LOGGER.info("HttpServerVerticle NOT deployed !!! " + res.cause());
                    }

                });

    }
}
