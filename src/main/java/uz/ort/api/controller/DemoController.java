package uz.ort.api.controller;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uz.ort.api.constants.VerticleUIDs;
import uz.ort.api.routers.RoutingService;
import uz.ort.api.utils.ResponseUtil;
import uz.ort.api.verticles.DemoDatabaseVerticle;

public class DemoController extends AbstructContoller {

    private static final Logger LOGGER = LoggerFactory.getLogger("DemoController");

    public DemoController(Vertx vertx) {
        super(vertx);
    }

    public void getList(RoutingContext context) {
        String uri = context.request().uri();

        LOGGER.info("getList :: " + uri);
        JsonObject params = getParams(context);


        this.vertx.eventBus().send(
                VerticleUIDs.DEMO_DATABASE_VERTICLE.val(),
                params,
                RoutingService.getDeliveryOptions("getList"),
                reply -> {
                    if (reply.succeeded()) {
                        JsonObject data = (JsonObject) reply.result().body();
                        ResponseUtil.respondJsonResult(context, data);
                    } else {
                        ResponseUtil.respondJsonError(context, "No data found");
                    }
                });
    }

}
