package uz.ort.api.utils;

import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import uz.ort.api.constants.ErrorCodes;

public class ResponseUtil {

    public static void respondJsonResult(RoutingContext routingContext, JsonObject content) {
        routingContext
                .response()
                .setStatusCode(200)
                .putHeader("content-type", "application/json; charset=utf-8")
                .putHeader("Location", routingContext.request().absoluteURI())
                .putHeader("Access-Control-Allow-Origin", "*")
                .putHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
                .end(new JsonObject().put("result", content).putNull("error").toString());
    }

    public static void respondJsonError(RoutingContext routingContext, String content) {
        routingContext
                .response()
                .setStatusCode(200)
                .putHeader("content-type", "application/json; charset=utf-8")
                .putHeader("Location", routingContext.request().absoluteURI())
                .putHeader("Access-Control-Allow-Origin", "*")
                .putHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
                .end(new JsonObject().put("error", content).putNull("result").toString());
    }

    public static void reportQueryError(Message<JsonObject> message, Throwable cause) {
        message.fail(ErrorCodes.DB_ERROR.val(), cause.getMessage());
    }
}
