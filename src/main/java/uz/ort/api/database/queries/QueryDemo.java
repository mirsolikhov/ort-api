package uz.ort.api.database.queries;

public class QueryDemo extends IQuery {
    @Override
    public String queryList() {
        return "" +
                " SELECT id, name, intval " +
                "   FROM public.demos " +
                "  WHERE status = 'A'; ";
    }

    @Override
    public String queryListByPage() {
        return "" +
                " SELECT id, name, intval " +
                "   FROM public.demos " +
                "  WHERE status = 'A' " +
                "  ORDER BY name" +
                "  LIMIT ? OFFSET ?; ";
    }

    @Override
    public String queryCountByPage() {
        return "" +
                " SELECT count(*) " +
                "   FROM public.demos " +
                "  WHERE status = 'A' " +
                "  ORDER BY name" +
                "  LIMIT ? OFFSET ?; ";
    }

    @Override
    public String queryById() {
        return "" +
                " SELECT id, name, intval " +
                "   FROM public.demos " +
                "  WHERE status = 'A'" +
                "    AND id = ? ; ";
    }

    @Override
    public String queryInsert() {
        return "" +
                " INSERT INTO public.demos" +
                "        (name, intval)" +
                " VALUES (?, ?); ";
    }

    @Override
    public String queryUpdate() {
        return "" +
                " UPDATE public.demos " +
                "   SET name = ?, intval = ?, mod_date = now() " +
                " WHERE id = ? " +
                "   AND status = 'A';";
    }

    @Override
    public String queryDelete() {
        return "" +
                " UPDATE public.demos " +
                "   SET status = 'D', exp_date = now() " +
                " WHERE id = ? " +
                "   AND status = 'A';";
    }
}
